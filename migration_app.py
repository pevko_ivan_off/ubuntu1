# Миграция с сервера на удаленный сервер
import os
import os.path
import select
import time
import paramiko
import subprocess
import sys
from paramiko import SSHClient
import logging
from logging import Formatter, StreamHandler
from os.path import abspath

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
handler = StreamHandler(stream=sys.stdout)
handler.setFormatter(Formatter(fmt='[%(asctime)s: %(levelname)s] %(message)s'))
logger.addHandler(handler)
logger.debug('Start migration...')

def sftpPutFile(host, port, user, pswd, fname, remotePath):
    logger.debug('Start copy file "{0}" to {1}'.format(fname, host))
    fExists = os.path.exists(fname)
    if fExists:
        try:
            trans = paramiko.Transport((host, int(port)))
            trans.connect(username=user, password=pswd)
            sftp = paramiko.SFTPClient.from_transport(trans)
            logger.debug('SFTP connection created')
        except Exception as e:
            logger.debug('Сервер {0} недоступен или введены неверные данные для подключения - {1}'.format(host,e))
            logger.debug('Migration Cancelled!')
            exit()
        else:
            localPath = os.path.abspath(os.path.dirname(__file__))+'/'+fname            
            # put файл
            sftp.put(localpath=localPath, remotepath=remotePath)
            fsize = os.path.getsize(localPath)
            logger.debug('File {0} size {1} bytes copy successfully'.format(remotePath, fsize))
            trans.close()
            logger.debug('SFTP connection closed')
    else:
        logger.debug('File {0} not found! Migration Cancelled!'.format(fname))
        exit()

def sftpGetFile(host, port, user, pswd, fname, remotePath):
    logger.debug('Start copy file "{0}" from {1}'.format(fname, host))
    try:
        trans = paramiko.Transport((host, int(port)))
        trans.connect(username=user, password=pswd)
        sftp = paramiko.SFTPClient.from_transport(trans)
        logger.debug('SFTP connection created')
    except Exception as e:
        logger.debug('Сервер {0} недоступен или введены неверные данные для подключения - {1}'.format(host,e))
        logger.debug('Migration Cancelled!')
        exit()
    else:
        remotePath = './'+fname
        localPath = os.path.abspath(os.path.dirname(__file__))+'/'+fname         
        fExists = os.path.exists(remotePath)
        # get файл
        sftp.get(remotepath=remotePath, localpath=localPath)
        fsize = os.path.getsize(localPath)
        logger.debug('File {0} size {1} bytes copy successfully'.format(localPath, fsize))
        trans.close()
        logger.debug('SFTP connection closed')

def commandRun(host, port, user, pswd, commText):
    try:
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(host, port, user, pswd)
    except Exception as e:
        logger.debug('Сервер {0} недоступен или введены неверные данные для подключения - {1}'.format(host, e))
        logger.debug('Migration Cancelled!')
        exit()
    else:
        ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(commText, get_pty=True) #, get_pty=True
        for stdoutrow in ssh_stdout:            
            logger.debug(stdoutrow)
        errs = ssh_stderr.read()
        if errs:
            raise Exception('Failed run command: {0}'.format(errs))            
            exit()
        while not ssh_stdout.channel.exit_status_ready():
            time.sleep(1)
            if ssh_stdout.channel.recv_ready():
                rl, wl, xl = select.select([ssh_stdout.channel], [], [], 0.0)
                if len(rl) > 0:
                    # Print data from stdout
                    logger.debug('-------------------------------')
                    logger.debug(ssh_stdout.channel.recv(1024))
                    logger.debug('-------------------------------')  
    finally:
        if ssh is not None:
            ssh.close()   

def deleteFile(host, port, user, pswd, fname):  
    fExists = os.path.isfile(fname)
    if fExists:
        try:
            trans = paramiko.Transport((host, int(port)))
            trans.connect(username=user, password=pswd)
            sftp = paramiko.SFTPClient.from_transport(trans)
            logger.debug('SFTP connection created')
        except Exception as e:
            logger.debug('Сервер {0} недоступен или введены неверные данные для подключения - {1}'.format(host,e))
            logger.debug('Migration Cancelled!')
            exit()
        else:
            try:
                os.remove(fname)
            except OSError as e: # Exception
                logger.debug("Failed with:", e.strerror)                 
            logger.debug('File {0} deleted successfully'.format(fname))
            trans.close()
            logger.debug('SFTP connection closed')        

def twoInOne(host, port, user, pswd, fname, remotePath, commText):
    sftpPutFile(host, port, user, pswd, fname, remotePath)
    logger.debug('Command run: "{0}"'.format(commText))
    commandRun(host, port, user, pswd, commText)
    
# step 1
logger.debug('Read settings old server...')
hostOld = input("Введите IP сервера с установленным Mark.Scan: ")
portOld = input("Введите порт сервера с установленным Mark.Scan: ")
userOld = input("Введите логин администратора: ")
pswdOld = input("Введите пароль администратора: ")
logger.debug('Old host {0}'.format(hostOld))

# step 2
twoInOne(hostOld, portOld, userOld, pswdOld, 'scripts/'+'dump.sh', 'dump.sh', 'chmod +x dump.sh; sed -i -e ''s/\r$//'' dump.sh; ./dump.sh')

# step 3
logger.debug('Read settings new server...')
hostNew = input("Введите IP нового сервера: ")
portNew = input("Введите порт нового сервера: ")
userNew = input("Введите логин администратора: ")
pswdNew = input("Введите пароль администратора: ")
logger.debug('New host {0}'.format(hostNew))

# step 4
choiceSystem = input("Требуется ли полная установка (1) на новый сервер {0} или только обновление данных (2)? Введите : ".format(hostNew))
while ((choiceSystem != '1') and (choiceSystem != '2')):
    choiceSystem = input("Требуется ли полная установка (1) на новый сервер {0} или только обновление данных (2)? Введите : ".format(hostNew))
if (choiceSystem == '2'):
    twoInOne(hostNew, portNew, userNew, pswdNew, 'scripts/'+'stop.sh', 'stop.sh', 'chmod +x stop.sh; sed -i -e ''s/\r$//'' stop.sh; ./stop.sh')
    logger.debug('Продолжается обновление данных...')
elif (choiceSystem == '1'):
    logger.debug('Установка на новый сервер продолжена...')

# step 5
commandRun(hostNew, portNew, userNew, pswdNew, 'cd')
sftpGetFile(hostOld, portOld, userOld, pswdOld, 'dump.sql', 'dump.sql')
sftpPutFile(hostNew, portNew, userNew, pswdNew, 'dump.sql', 'dump.sql')
commandRun(hostNew, portNew, userNew, pswdNew, 'mkdir /opt/mark-scan; cd /opt/mark-scan')
sftpPutFile(hostNew, portNew, userNew, pswdNew, 'scripts/.env', '/opt/mark-scan/.env')
sftpPutFile(hostNew, portNew, userNew, pswdNew, 'scripts/.jobber', '/opt/mark-scan/.jobber')
sftpPutFile(hostNew, portNew, userNew, pswdNew, 'scripts/docker-compose.yaml', '/opt/mark-scan/docker-compose.yaml')
sftpPutFile(hostNew, portNew, userNew, pswdNew, 'scripts/Makefile', '/opt/mark-scan/Makefile')
commandRun(hostNew, portNew, userNew, pswdNew, 'cd /opt/mark-scan; chmod 600 .jobber')

# step 6
if (choiceSystem == '1'):
    logger.debug('Install docker begin...')
    commandRun(hostNew, portNew, userNew, pswdNew, 'cd /etc/apt/keyrings/; rm -f docker.gpg; cd')        
    twoInOne(hostNew, portNew, userNew, pswdNew, 'scripts/'+'docker.sh', 'docker.sh', 'chmod +x docker.sh; sed -i -e ''s/\r$//'' docker.sh; ./docker.sh')    
    logger.debug('Install docker end...')

# step 7
twoInOne(hostNew, portNew, userNew, pswdNew, 'scripts/'+'init.sh', 'init.sh', 'chmod +x init.sh; sed -i -e ''s/\r$//'' init.sh; ./init.sh')

logger.debug('End migration to {0}'.format(hostNew))