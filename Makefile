#!make
include .env
export $(shell sed 's/=.*//' .env)

up:
	@docker compose up -d

down:
	@docker compose down -v

restart:
	@$(MAKE) down
	@$(MAKE) up

init:
	@$(MAKE) down
	@rm -rf .service
	@rm -rf .goods-service
	@rm -rf .print-service
	@rm -rf .cron
	@$(MAKE) up
	@sleep 5
	@docker exec mark-scan-db sh -c 'psql -p $$PGPORT -U $$POSTGRES_USER $$POSTGRES_DB < /app/dump.sql'
	@docker exec mark-scan-db sh -c 'psql -p $$PGPORT -U $$POSTGRES_USER $$POSTGRES_DB < /app/update_to_local.sql'
	@docker exec mark-scan-goods-db sh -c 'psql -p $$PGPORT -U $$POSTGRES_USER $$POSTGRES_DB < /app/dump.sql'
	@docker exec mark-scan-print-db sh -c 'psql -p $$PGPORT -U $$POSTGRES_USER $$POSTGRES_DB < /app/dump.sql'
	@sleep 3
	@$(MAKE) restart

db.dump:
	@docker exec mark-scan-db sh -c 'pg_dump -p $$PGPORT -U $$POSTGRES_USER -d $$POSTGRES_DB -F p > /var/lib/postgresql/data/dump.sql'

db.goods.dump:
	@docker exec mark-scan-goods-db sh -c 'pg_dump -p $$PGPORT -U $$POSTGRES_USER -d $$POSTGRES_DB -F p > /var/lib/postgresql/data/dump.sql'

db.print.dump:
	@docker exec mark-scan-print-db sh -c 'pg_dump -p $$PGPORT -U $$POSTGRES_USER -d $$POSTGRES_DB -F p > /var/lib/postgresql/data/dump.sql'

db.restore:
	@docker exec mark-scan-db sh -c 'psql -p $$PGPORT -U $$POSTGRES_USER $$POSTGRES_DB < /var/lib/postgresql/data/dump.sql'
	@sleep 1
	@$(MAKE) restart	

log.service:
	@docker logs mark-scan-service

log.db:
	@docker logs mark-scan-db

log.goods.service:
	@docker logs mark-scan-goods-service

log.goods.db:
	@docker logs mark-scan-goods-db

log.print.service:
	@docker logs mark-scan-print-service

log.print.db:
	@docker logs mark-scan-print-db

log.proxy:
	@docker logs mark-scan-proxy

log.cron:
	@docker logs mark-scan-cron
